var fs = require('fs');
var cheerio = require('cheerio');
var path = require('path');

var removeScripts = function (path) {
  
  var data = fs.readFileSync(path, 'utf8');
  var $ = cheerio.load(data);
  $('script').remove();
  console.log($.html());
  fs.writeFileSync(path, $.html());
};

fs.readdir('ms.mcmaster.ca/~haradam/', function(err, files) {
  if (err) return console.log(err);
  
  files.forEach(function(f) {
      if (path.extname(f) === ".html") { 
        console.log('Files: ' + f);
        removeScripts('ms.mcmaster.ca/~haradam/'+f);
      }
  });
});