serverside html parsing via node.js + cheerios
using meg's site as data

required:
    npm install cheerios
    
Onetime Migration Setup
Step 0 
    Set up nodejs. Install cheerios. Run tests.

Step 1
    run clone-site.sh to clone the current site
    
Step 2
    edit links.html to update the links if necessary before running "node updateindex.js" or 
    manually edit index.html page.
    
Step 3
    run "node removescripts.js" to remove js scripts in all html pages.
    

"Daily" routine (i.e. Edit and Publish)
Step 0
    edit html tags, spellings, etc
    if new html page, edit the navigational links on the index.html page
    
Step 1
    node synclinks.js to copy the links in the index to other html pages
    
Step 2
    deploy *.html pages


