var fs = require('fs');
var cheerio = require('cheerio');

fs.readFile('links.html', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  var links = data;//new links to replace #linkList in index.html
  //console.log(links);
  
  fs.createReadStream('ms.mcmaster.ca/~haradam/index.html') .pipe(fs.createWriteStream('ms.mcmaster.ca/~haradam/index.html.bak'));
  
  //read index.html to be modified
  fs.readFile('ms.mcmaster.ca/~haradam/index.html.bak', 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    var $ = cheerio.load(data);
    
    $('#linkList').replaceWith(links);//do not use html("...") adds as child
    console.log($.html());
    //write over index.html with updated DOM
    fs.writeFile('ms.mcmaster.ca/~haradam/index.html', $.html(), function (err) {
      if (err) return console.log(err);
    }); 
  });
});