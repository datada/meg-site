var fs = require('fs');
var cheerio = require('cheerio');
var path = require('path');

var updateDOM = function (path, domID, htmlet) {
  
  var data = fs.readFileSync(path, 'utf8');
  var $ = cheerio.load(data);
  $(domID).html(htmlet);//replace the content string!!!
  console.log($.html());
  fs.writeFileSync(path, $.html());
};

fs.readFile('ms.mcmaster.ca/~haradam/index.html', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  var $ = cheerio.load(data);
  var links = $('#linkList').html();//get the content string
  console.log(links);
  
  fs.readdir('ms.mcmaster.ca/~haradam/', function(err, files) {
    if (err) return console.log(err);
    
    files.forEach(function(f) {
        if (path.extname(f) === ".html") { 
          console.log('Files: ' + f);
          updateDOM('ms.mcmaster.ca/~haradam/'+f,'#linkList', links);
        }
    });
  });
});